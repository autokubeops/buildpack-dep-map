package getter

import (
	"gitlab.com/autokubeops/buildpack-dep-map/internal/cnb"
	"gitlab.com/autokubeops/buildpack-dep-map/internal/work"
	"log"
	"net/url"
	"strings"
)

func (m *Manager) BuildPack(name string) ([]*cnb.Dependency, error) {
	var files []*cnb.Dependency
	var buildpack cnb.BuildPack
	if err := work.GetTOML(name, cnb.ManifestBuildPack, &buildpack); err != nil {
		return nil, err
	}
	for _, d := range buildpack.Metadata.Dependencies {
		files = append(files, m.rewrite(d))
	}
	for _, o := range buildpack.Orders {
		for _, g := range o.Groups {
			f, err := m.BuildPack(g.ID)
			if err != nil {
				continue
			}
			files = append(files, f...)
		}
	}
	return files, nil
}

func (m *Manager) rewrite(dependency cnb.Dependency) *cnb.Dependency {
	if m.uri == "" {
		return &dependency
	}
	uri, err := url.Parse(dependency.URI)
	if err != nil {
		log.Printf("failed parse uri (%s): %s", dependency.URI, err)
		return &dependency
	}
	dependency.URI = strings.ReplaceAll(dependency.URI, uri.Host, m.uri)
	return &dependency
}
