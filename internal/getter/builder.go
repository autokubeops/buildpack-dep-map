package getter

import (
	"github.com/briandowns/spinner"
	"gitlab.com/autokubeops/buildpack-dep-map/internal/cnb"
	"gitlab.com/autokubeops/buildpack-dep-map/internal/work"
	"log"
	"time"
)

func (m *Manager) Builder(name string) ([]*cnb.Dependency, error) {
	log.Printf("scanning builder: %s", name)
	s := spinner.New(spinner.CharSets[9], time.Millisecond*150)
	s.Start()
	var files []*cnb.Dependency
	var builder cnb.Builder
	if err := work.GetTOML(name, cnb.ManifestBuilder, &builder); err != nil {
		return nil, err
	}
	for _, o := range builder.Orders {
		for _, g := range o.Groups {
			f, err := m.BuildPack(g.ID)
			if err != nil {
				continue
			}
			files = append(files, f...)
		}
	}
	s.Stop()
	log.Printf("fetched %d dependencies for builder", len(files))
	return files, nil
}
