package getter

import "strings"

type Manager struct {
	uri string
}

func NewManager(uri string) *Manager {
	return &Manager{
		uri: strings.TrimSuffix(uri, "/"),
	}
}
