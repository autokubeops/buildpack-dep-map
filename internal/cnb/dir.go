package cnb

import (
	"github.com/cheggaaa/pb"
	"log"
	"os"
	"path/filepath"
)

func InitDir() error {
	if err := os.MkdirAll("./bindings/dependency-mapping/binaries", 0744); err != nil {
		log.Printf("failed to ensure directory: %s", err)
		return err
	}
	if err := os.WriteFile("./bindings/dependency-mapping/type", []byte("dependency-mapping"), 0644); err != nil {
		log.Printf("failed to create binding type: %s", err)
	}
	return nil
}

func WriteDependencies(files []*Dependency) error {
	log.Printf("writing files")
	bar := pb.StartNew(len(files))
	for _, f := range files {
		if err := os.WriteFile(filepath.Join("./bindings/dependency-mapping", f.SHA256), []byte(f.URI), 0644); err != nil {
			log.Printf("failed to write file: %s", err)
			return err
		}
		bar.Increment()
	}
	return nil
}
