package cnb

const (
	ManifestBuilder   = "builder"
	ManifestBuildPack = "buildpack"
)

type Dependency struct {
	URI    string `toml:"uri"`
	SHA256 string `toml:"sha256"`
}

type Metadata struct {
	Dependencies []Dependency `toml:"dependencies"`
}

type Group struct {
	ID string `toml:"id"`
}

type Order struct {
	Groups []Group `toml:"group"`
}

type Builder struct {
	Orders []Order `toml:"order"`
}

type BuildPack struct {
	Builder
	Metadata Metadata `toml:"metadata"`
}
