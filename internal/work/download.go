package work

import (
	"errors"
	"fmt"
	"github.com/BurntSushi/toml"
	"gitlab.com/autokubeops/buildpack-dep-map/internal/cnb"
	"log"
	"net/http"
)

var (
	ErrRequestFailed = errors.New("request failed due to unexpected status code")
)

func GetTOML[T *cnb.Builder | *cnb.BuildPack](builder, filename string, v T) error {
	target := fmt.Sprintf("https://raw.githubusercontent.com/%s/main/%s.toml", builder, filename)
	//log.Printf("fetching file: %s", target)
	resp, err := http.Get(target)
	if err != nil {
		log.Printf("failed to execute request: %s", err)
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Printf("request failed with code: %d", resp.StatusCode)
		return ErrRequestFailed
	}
	if _, err := toml.NewDecoder(resp.Body).Decode(v); err != nil {
		log.Printf("failed to read TOML: %s", err)
		return err
	}
	return nil
}
