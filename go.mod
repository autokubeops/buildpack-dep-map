module gitlab.com/autokubeops/buildpack-dep-map

go 1.18

require (
	github.com/BurntSushi/toml v1.0.0 // indirect
	github.com/briandowns/spinner v1.18.1 // indirect
	github.com/cheggaaa/pb v1.0.29 // indirect
	github.com/fatih/color v1.9.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/spf13/cobra v1.4.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
)
