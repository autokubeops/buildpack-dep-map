# BuildPack Dependency Map

This project provides a CLI tool for creating Paketo-style Dependency Mappings for enabling offline CloudNative BuildPacks.

For more information on Dependency Mappings, take a look at the [Paketo documentation](https://paketo.io/docs/howto/configuration/#dependency-mappings).

## Getting started

1. Download the [latest release](https://gitlab.com/autokubeops/buildpack-dep-map/-/releases)
2. Place the binary somewhere on your `PATH` (e.g. `~/.local/bin`)

## Usage

```shell
bdm --name paketo-buildpacks/full-builder --url my-binary-store.example.org
```

BDM will traverse all the BuildPacks in the "full-builder" Builder and create a dependency mapping.
You will find all the files in the `bindings` directory.

The `--url` flag instructs BDM to replace the source hostname (almost always `deps.paketo.io`) with one of your choosing.
The flag can include a path, but must not include the scheme.

For example:

```shell
--url artifactory.example.org/artifactory/deps-paketo-io
```
