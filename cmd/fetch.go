/*
Copyright © 2022 Django Cass django@dcas.dev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cmd

import (
	"gitlab.com/autokubeops/buildpack-dep-map/internal/cnb"
	"gitlab.com/autokubeops/buildpack-dep-map/internal/getter"
	"log"

	"github.com/spf13/cobra"
)

// fetchCmd represents the fetch command
var fetchCmd = &cobra.Command{
	Use:   "fetch",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		_ = cnb.InitDir()
		name := cmd.Flag("name").Value.String()
		files, err := getter.NewManager(cmd.Flag("url").Value.String()).Builder(name)
		if err != nil {
			log.Fatalln("command failed due to error")
			return
		}
		if err := cnb.WriteDependencies(files); err != nil {
			log.Fatalln("command failed due to error")
			return
		}
	},
}

func init() {
	rootCmd.AddCommand(fetchCmd)

	fetchCmd.Flags().StringP("name", "n", "", "name of the builder to fetch")
	fetchCmd.Flags().StringP("url", "u", "", "url to use instead of the default (e.g. deps.paketo.io -> deps.mycompany.local)")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// fetchCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// fetchCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
